import * as firebase from 'firebase'

const firebaseConfig = {
  apiKey: "AIzaSyBp5eSxD_CMP253tYdJcldp92dNXexad3U",
  authDomain: "practica02-6b476.firebaseapp.com",
  databaseURL: "https://practica02-6b476.firebaseio.com",
  projectId: "practica02-6b476",
  storageBucket: "practica02-6b476.appspot.com",
  messagingSenderId: "820383631617",
  appId: "1:820383631617:web:1ec45b72b7142b6bd83300"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

export default firebase