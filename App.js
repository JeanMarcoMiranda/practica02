/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import { Button, View, Text, Image, StyleSheet } from 'react-native';
import 'react-native-gesture-handler'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack';

//My Components
import Tabs from './app/components/tabs'
import Login from './app/components/UserRegister/login'
import Register from './app/components/UserRegister/register'

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ 
            headerShown: false
          }}
        />
        <Stack.Screen
          name="Register"
          component={Register}
          options={{ 
            headerShown: false
          }}
        />
        <Stack.Screen
          name="Main"
          component={Tabs}
          options={{ 
            headerShown: false
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default App;
