var moment = require('moment');
moment.locale('es');

import React, {Component} from 'react';
import {Text, View, Button, TextInput, TouchableOpacity, Switch} from 'react-native';
import firebase from '../../../firebase'
import 'firebase/firestore'

// DECLARAR LAS LIBRERIAS DE LOS COMPONENTES INSTALADOS
import ModalSelector from 'react-native-modal-selector'
import MaterialsIcon from 'react-native-vector-icons/MaterialIcons';
import { Kohana } from 'react-native-textinput-effects';
import Style from './Style'
import DateTimePickerModal from "react-native-modal-datetime-picker"

let index = 0

const data = [
   { key: index++, section: true, label: 'Cuentas' },
]

export default class TransferenceFirst extends Component {
   constructor(props) {
      super(props);
      this.state = {
         inputOrigen: '',
         inputDestino: '',
         importe: '',
         referencia: '',
         fecha: 'mm/dd/yy',
         switchEnabled: false,
         isDatePickerVisible: false,

         errorInputOrigen: false,
         errorInputDestino: false,
         errorImporte: false,
         errorReferencia: false,
         errorFecha: false,
         errorTxtInputDestino: '',
      };
   }

   componentDidMount() {
      const db = firebase.firestore()
      db.settings({experimentalForceLongPolling: true})

      const userRef = db.collection('users')
      userRef.get()
         .then( snapshot => {
            snapshot.forEach(doc => {
               console.log(doc.id, '=>', doc.data())
               data.push({
                  key: index++,
                  label: doc.data().accountNumber
               })
            })
         })
         .catch( err => {
            console.log('Error getting docs', err)
         })
      
   }
   

   changeImporte = text => {
      let regexNum = /^[0-9]+$/
      if(regexNum.test(text)) {
         this.setState({importe:text})
      }else if (text === '') {
         this.setState({importe:text})
      }
   }

   changeReferencia = text => {
      this.setState({referencia:text})
   }

   toggleSwitch = () => {
      let state = !this.state.switchEnabled
      this.setState({switchEnabled:state})
   }

   showDatePicker = () => {
      this.setState({isDatePickerVisible:true})
   };

   hideDatePicker = () => {
      this.setState({isDatePickerVisible:false});
   };

   handleConfirm = (date) => {
      let fecha = moment(date).format('DD/MM/yyyy')
      console.log("A date has been picked: ", fecha);
      this.setState({fecha: fecha})
      this.hideDatePicker()
   };

   errOrigen = () => {
      this.setState({
         errorInputOrigen: true,
      })
   }

   errDestino = text => {
      this.setState({
         errorInputDestino: true,
         errorTxtInputDestino: text,
      })
   }

   errImporte = () => {
      this.setState({
         errorImporte: true,
      })
   }

   errReferencia = () => {
      this.setState({
         errorReferencia: true,
      })
   }

   errFecha = () => {
      this.setState({
         errorFecha: true,
      })
   }

   noErr = () => {
      this.setState({
         errorInputOrigen: false,
         errorInputDestino: false,
         errorImporte: false,
         errorReferencia: false,
         errorFecha: false,
      })
   }

   validateForm = () => {
      console.log(data)
      let errores = 0
      this.noErr()
      if(this.state.inputOrigen === this.state.inputDestino) {
         this.errDestino("Seleccione una Cuenta distinta")
         errores++
      }
      if(this.state.inputOrigen === '') {
         this.errOrigen()
         errores++
      }
      if(this.state.inputDestino === '') {
         this.errDestino("Seleccione una Cuenta")
         errores++
      }
      if(this.state.importe === '') {
         this.errImporte()
         errores++
      }
      if(this.state.referencia === '') {
         this.errReferencia()
         errores++
      }
      if(this.state.fecha === 'mm/dd/yy') {
         this.errFecha()
         errores++
      }

      if(errores == 0){
         this.props.navigation.navigate('Second', {
            origen: this.state.inputOrigen,
            destino: this.state.inputDestino,
            importe: this.state.importe,
            referencia: this.state.referencia,
            fecha: this.state.fecha,
            notificar: this.state.switchEnabled,
         })
      }  
   }

   render() {
      // let index = 0;
      // const data = [
      //    { key: index++, section: true, label: 'Cuentas' },
      //    { key: index++, label: '000000000231456' },
      //    { key: index++, label: '000000000231455' },
      // ];
   return (
      <View style={{flex: 1, backgroundColor:'white', paddingTop: 50}}>
         <View style={Style.viewModalContainer}>
            <Text style={Style.subText}>Cuenta Origen:</Text>
            <ModalSelector
               data={data} 
               initValue="Selecciona una Cuenta"
               supportedOrientations={['landscape']}
               accessible={true}
               scrollViewAccessibilityLabel={'Scrollable options'}
               cancelButtonAccessibilityLabel={'Cancel Button'}
               onChange={(option)=>{ this.setState({inputOrigen:option.label})}}>
               <View style={Style.viewModal}>
                  <TextInput
                     style={Style.inputModal}
                     editable={false}
                     placeholder="Selecciona una Cuenta"
                     value={this.state.inputOrigen} />
               </View>
            </ModalSelector>
         </View>
         {this.state.errorInputOrigen ? <Text style={Style.errorText}>Seleccione una Cuenta</Text> : null}

         <View style={Style.viewModalContainer}>
            <Text style={Style.subText}>Cuenta Destino:</Text>
            <ModalSelector
               data={data}
               initValue="Selecciona una Cuenta"
               supportedOrientations={['landscape']}
               accessible={true}
               scrollViewAccessibilityLabel={'Scrollable options'}
               cancelButtonAccessibilityLabel={'Cancel Button'}
               onChange={(option)=>{ this.setState({inputDestino:option.label})}}>
               <View style={Style.viewModal}>
                  <TextInput
                     style={Style.inputModal}
                     editable={false}
                     placeholder="Selecciona una Cuenta"
                     value={this.state.inputDestino} />
               </View>
               
            </ModalSelector>
         </View>
         {this.state.errorInputDestino ? <Text style={Style.errorText}>{this.state.errorTxtInputDestino}</Text> : null}

         <View style={Style.viewInputContainer}>
            <Text style={Style.subText}>Importe:</Text>
            <Kohana
               style={Style.inputKohana}
               label={'Importe'}
               iconClass={MaterialsIcon}
               iconName={'credit-card'}
               iconColor={'#3B77DB'}
               inputPadding={5}
               editable={true}
               labelStyle={{ color: '#6A6E7B', fontSize: 16 }}
               inputStyle={{ color: '#6A6E7B', fontSize: 16 }}
               labelContainerStyle={{ padding: 4 }}
               iconContainerStyle={{ padding: 10 }}
               useNativeDriver
               onChangeText={text => this.changeImporte(text)}
               value={this.state.importe}
            />
         </View>
         {this.state.errorImporte ? <Text style={Style.errorText}>Coloque un importe</Text> : null}

         <View style={Style.viewInputContainer}>
            <Text style={Style.subText}>Referencia:</Text>
            <Kohana
               style={Style.inputKohana}
               label={'Referencia'}
               iconClass={MaterialsIcon}
               iconName={'account-balance-wallet'}
               iconColor={'#3B77DB'}
               inputPadding={5}
               editable={true}
               labelStyle={{ color: '#6A6E7B', fontSize: 16 }}
               inputStyle={{ color: '#6A6E7B', fontSize: 16 }}
               labelContainerStyle={{ padding: 4 }}
               iconContainerStyle={{ padding: 10 }}
               useNativeDriver
               onChangeText={text => this.changeReferencia(text)}
               value={this.state.referencia}
            />
         </View>
         {this.state.errorReferencia ? <Text style={Style.errorText}>Coloque una referencia</Text> : null}

         <View style={Style.viewInputContainer}>
            <Text style={Style.subText}>Fecha:</Text>
            <TouchableOpacity style={Style.viewDatePicker} onPress={this.showDatePicker}>
               <Text style={Style.inputFecha}>{this.state.fecha}</Text>
            </TouchableOpacity>
            <DateTimePickerModal
               isVisible={this.state.isDatePickerVisible}
               mode="date"
               onConfirm={this.handleConfirm}
               onCancel={this.hideDatePicker}
            />
         </View>
         {this.state.errorFecha ? <Text style={Style.errorText}>Seleccione una Fecha</Text> : null}

         <View style={[Style.viewInputContainer,{height: 40}]}>
            <View style={Style.switchView}>
               <Text style={Style.switchText}>Notificar al correo: </Text>
               <Switch
                  trackColor={{ false: "#767577", true: "#81b0ff" }}
                  thumbColor={this.state.switchEnabled ? "#3B77DB" : "#f4f3f4"}
                  ios_backgroundColor="#3e3e3e"
                  onValueChange={this.toggleSwitch}
                  value={this.state.switchEnabled}
               />
            </View>
         </View>

         <View style={Style.viewInputContainer}>
            <Button
               color="#4285F4"
               title="Siguiente"
               onPress={this.validateForm}
            />
         </View>
         </View>
      );
   }
}