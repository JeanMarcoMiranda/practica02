import { StyleSheet } from "react-native"

const Style = StyleSheet.create({
   viewModalContainer: {
      marginVertical:5,
      marginHorizontal: 40,
      height: 60
   },
   viewInputContainer: {
      //backgroundColor: 'pink',
      marginVertical:5,
      marginHorizontal: 40,
      height: 60
   },
   viewModal: {
      backgroundColor:'#E5E5E5',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      borderRadius:10
   },
   viewDatePicker: {
      alignSelf: "center",
      width: 100,
      backgroundColor:'#E5E5E5',
      borderRadius:10
   },
   inputModal: {
      flex:1,
      textAlign: 'center',
      textAlignVertical:"center",
      height: 41,
      fontWeight:'bold',
      fontSize: 16,
   },
   inputKohana: {
      backgroundColor: '#E5E5E5',
      borderRadius:10
   },
   inputFecha: {
      color: '#6A6E7B',
      textAlign: 'center',
      textAlignVertical:"center",
      height: 41,
      fontWeight:'bold',
      fontSize: 16,
   },
   subText: {
      color: '#6A6E7B'
   },
   switchText: {
      color: '#6A6E7B',
      textAlignVertical: "center",
   },
   switchView: {
      flexDirection: 'row',
      justifyContent: 'flex-start',
      alignItems: 'center'
   },
   errorText: {
      color:'red',
      fontSize: 12,
      textAlign: 'center',
      marginTop: -5,
      marginBottom: -6,
   },
   itemSeparator: {
      height: 2,
      width: '80%',
      alignSelf: 'center',
      backgroundColor: '#4285F4',
   }
})

export default Style