import React, {Component} from 'react';
import {Text, View, Button} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'

export default class TransferenceThird extends Component {
  constructor(props) {
    super(props);
    this.state = {
      importe: 0,
    };
  }

  render() {
    return (
      <View style={{height:'100%', backgroundColor:'white', paddingTop: 150, alignItems:'center'}}>
        <MaterialCommunityIcons name="check-decagram" color={'#45B746'} size={200} />
        <View style={{width:200, marginTop: 30}}>
          <Button
            color="#3B77DB"
            title="Aceptar"
            onPress={() => this.props.navigation.popToTop()}
          />
        </View>
      </View>
    );
  }
}

/*
  CREAR SUS ESTILOS
  */
