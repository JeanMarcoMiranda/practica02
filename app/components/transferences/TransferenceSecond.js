var moment = require('moment');
moment.locale('es');

import React, {Component} from 'react';
import {Text, View, Button, FlatList} from 'react-native';
import Style from './Style'
import firebase from '../../../firebase'
import 'firebase/firestore'

function Item({title, value}) {
  return(
    <View style={Style.viewInputContainer}>
      <Text style={Style.subText}>{title} </Text>
      <Text style={Style.inputFecha}>{value}</Text>
    </View>
  )
}

export default class TransferenceSecond extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [{
        id:1,
        text:'Cuenta Origen:',
        value: this.props.route.params.origen
      },
      {
        id:2,
        text:'Cuenta Destino:',
        value: this.props.route.params.destino
      },
      {
        id:3,
        text:'Importe:',
        value: this.props.route.params.importe
      },
      {
        id:4,
        text:'Referencia:',
        value: this.props.route.params.referencia
      },
      {
        id:5,
        text:'Fecha:',
        value: this.props.route.params.fecha
      },
      {
        id:6,
        text:'Cuenta Origen:',
        value: this.props.route.params.notificar ? 'SI' : 'NO'
    }]}
  }

   /*
  CREAR AQUI FUNCIONES PARA NAVEGAR 'VOLVER' y 'CONFIRMAR'
  */
  FlatListSeparator = () => {
    return(
      <View style={Style.itemSeparator} />
    )
  }

  onPressConfirm = () => {
    const db = firebase.firestore()
    db.settings({experimentalForceLongPolling: true})

    let origenData = {}
    let destinoData = {}

    const userRef = db.collection('users')
    userRef.where('accountNumber', '==', this.props.route.params.origen).get()
      .then( snapshot => {
        snapshot.forEach(doc => {
            console.log(doc.id, ' origen =>', doc.data())
            origenData = doc.data()
            origenData.lastTransfer = this.props.route.params.fecha + ' ' + this.props.route.params.referencia
            origenData.bill = (Number(origenData.bill) - Number(this.props.route.params.importe)).toString()

            userRef.doc(doc.id).set(origenData)
        })
      })
      .catch( err => {
        console.log('Error getting docs', err)
      })

    userRef.where('accountNumber', '==', this.props.route.params.destino).get()
      .then( snapshot => {
        snapshot.forEach(doc => {
            console.log(doc.id, ' destino =>', doc.data())
            destinoData = doc.data()
            destinoData.lastTransfer = this.props.route.params.fecha + ' ' + this.props.route.params.referencia
            destinoData.bill = (Number(destinoData.bill) + Number(this.props.route.params.importe)).toString()
            
            userRef.doc(doc.id).set(destinoData)
        })
      })
      .catch( err => {
        console.log('Error getting docs', err)
      })

    this.props.navigation.navigate('Third')
  }

  render() {
    return (
      <View style={{height:'100%', backgroundColor:'white', paddingTop: 40}}>
        <FlatList
          style={{flex:2}}
          data={this.state.data.length > 0 ? this.state.data : []}
          keyExtractor={item => item.id.toString()}
          ItemSeparatorComponent={this.FlatListSeparator}
          renderItem={({item}) => (
            <Item 
                title={item.text}
                value={item.value}
            />
          )}
        />

        <View style={[Style.switchView, {marginHorizontal:35, justifyContent:"center",flex:.3}]}>
          <View style={{marginHorizontal:20}}>
            <Button
              color="#3B77DB"
              title="Volver"
              onPress={() => this.props.navigation.popToTop()}
            />
          </View>
          <Button
            color="#4285F4"
            title="Confirmar"
            onPress={this.onPressConfirm}
          />
        </View>
      </View>
    );
  }
}

/*
  CREAR SUS ESTILOS
  */
