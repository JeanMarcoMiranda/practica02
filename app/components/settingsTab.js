import React, {useState} from 'react'
import { View, Text, StyleSheet, FlatList, TouchableHighlight } from 'react-native'
import { SearchBar } from 'react-native-elements'

//My Commponents
import SettingItem from './settingsListItem'

const Settings = () => {

    const datos = [
        {
            id: 1,
            icon: 'user',
            title: 'Account',
        },
        {
            id: 2,
            icon: 'bell',
            title: 'Notifications',
        },
        {
            id: 3,
            icon: 'eye',
            title: 'Appearence',
        },
        {
            id: 4,
            icon: 'lock',
            title: 'Privacy & Security',
        },
        {
            id: 5,
            icon: 'headphones',
            title: 'Help & Support',
        },
        {
            id: 6,
            icon: 'questioncircleo',
            title: 'About',
        }
    ]

    const [data, setData] = useState(datos)
    const [searchText, setSearchText] = useState('')

    const filtrado = () => {
        console.warn('typed')
        /* const dataFiltered = data.filter((item) => {
            return item.iconTitle.includes(searchText)
        })
        setData(dataFiltered) */
    }


    return (
        <View style={style.padre}>
            <View>
                <SearchBar
                    lightTheme
                    onChangeText={filtrado}
                    onClearText={filtrado}
                    icon={{ type: 'font-awesome', name: 'search' }}
                    placeholder='Type Here...' />
            </View>
            <View style={style.listContainer}>
                <FlatList
                    style={{ height: '100%' }}
                    keyExtractor={(item) => item.id}
                    data={data}
                    renderItem={({ item, separators }) => (
                        <TouchableHighlight
                            activeOpacity={0.8}
                            underlayColor="#221C2E"
                            /* onPress={() => {
                                this.props.navigation.navigate('Details', item)
                            }} */
                            onShowUnderlay={separators.highlight}
                            onHideUnderlay={separators.unhighlight}
                            style={{
                                borderRadius: 15,
                            }}
                        >
                            <SettingItem iconName={item.icon} iconTitle={item.title}/>
                        </TouchableHighlight>
                    )}
                />
            </View>
        </View>
    )
}

const style = StyleSheet.create({
    title: {
        color: '#2ECBE4',
        fontSize: 20,
        textAlign: 'center'
    },
    listContainer: {
        width: '100%',
        height: '100%',
        padding: 20,
        paddingTop: 5,
    },
    padre: {
        flex: 1,
        width: '100%',
        backgroundColor: '#E8E8EA'
    },
    containerPadre: {
        flex: 1,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#171520',
        margin: 10,
        alignItems: 'center',
        paddingTop: 5,
        backgroundColor: '#F4F5F6'
    },

})

export default Settings