import React from 'react'
import { View, Text, StyleSheet, Image} from 'react-native'

const ListItem = props => {
    return (
        <View style={style.listItemContainer}>
            <View style={style.listItemImageContainer}>
                <Image
                    style={style.cover}
                    source={{ uri: props.dataItem.image }}
                />
            </View>
            <View style={style.listItemDesciptionContainer}>
                <Text style={{ color: '#80BD9D' }}>Nombre: <Text style={{ color: '#F98766' }}>{props.dataItem.name}</Text></Text>
                <Text style={{ color: '#80BD9D' }}>Género: <Text style={{ color: '#F98766' }}>{props.dataItem.gender}</Text></Text>
                <Text style={{ color: '#80BD9D' }}>Especie: <Text style={{ color: '#F98766' }}>{props.dataItem.species}</Text></Text>
            </View>
        </View>
    )
}

const style = StyleSheet.create({
    listItemContainer: {
        flex: 1,
        flexDirection: 'row',
        marginBottom: 10,
        marginTop: 10,
        justifyContent: 'flex-start',
        padding: 2
    },
    listItemImageContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: "center",
        alignItems: 'center',
        width: '50%',
        padding: 5,
    },
    listItemDesciptionContainer: {
        width: '50%',
        padding: 15
    },
    cover: {
        borderRadius: 50,
        borderColor: '#DFE5F0',
        resizeMode: 'contain',
        height: 85,
        width: 85,
    }
})

export default ListItem