import React from 'react'
import {View, Text, StyleSheet} from 'react-native'
import MaterialIcons from 'react-native-vector-icons/Feather'
import AntDesign from 'react-native-vector-icons/AntDesign'

const SettingItem = (props) => {
  let icon;
  (props.iconName === 'questioncircleo')
  ? icon = <AntDesign name={props.iconName} size={26}/>
  : icon = <MaterialIcons name={props.iconName} size={26}/>

  return(
    <View style={style.itemContainer}>
      <View style={style.itemIcon}>
        {icon}
      </View>
      <View style={style.itemTitle}>
        <Text>{props.iconTitle}</Text>
      </View>
    </View>
  )
}

const style = StyleSheet.create({
  itemContainer:{
    flex: 1,
    flexDirection: 'row',
    height: '10%',
    width: '100%',
  },
  itemIcon:{
    padding: 10,
    width: '12%',
  },
  itemTitle:{
    flex: 1,
    justifyContent: 'center',
    padding: 10,
    width: '70%',
  }
})
{/* <View style={style.itemContainer}>
      <View style={style.itemIcon}>
        <MaterialIcons/>
      </View>
      <View style={style.itemTitle}>
        <Text></Text>
      </View>
    </View> */}
export default SettingItem