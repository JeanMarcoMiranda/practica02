import React, {useState, useRef, useEffect } from 'react'
import { Image, TextInput, ImageBackground, Dimensions, View, Text, StyleSheet, FlatList, TouchableHighlight, SafeAreaView, ScrollView, TouchableOpacity, StatusBar } from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Carousel from 'react-native-anchor-carousel'
//My Commponents
const { height } = Dimensions.get('window');
const box_count = 3;
const box_height = height / box_count;

const Home = () => {

const [background, setBackground] = useState({
    uri: 'https://c0.wallpaperflare.com/preview/1017/538/491/money-finance-bank-cash.jpg',
    name: 'Banco de la Nacion Bruxx-Sparrow',
    stat: '2019 - Arequipa - Perú',
    desc: 'Brindar soluciones financieras con calidad de atención, agregando valor, contribuyendo conla descentralización, ampliando nuestra cobertura de servicios y promoviendo la bancarización con inclusión social.',
})

const [gallery, setGallery] = useState([
    {
    image: 'https://www.bn.com.pe/noticias/imagenes/2020/edificio-bn.jpg',
    title: 'Reprogramacion de creditos',
    released: '2019 - Arequipa - Perú',
    desc: 'Si tienes un cuenta en el Banco de la Nación, podrás manejarla desde tu Smartphone utilizando su aplicación móvil (App) para realizar distintas operaciones.',
    key: '1',
    },
    {
    image: 'https://cdn.hipwallpaper.com/i/20/62/ZbBgvl.jpg',
    title: 'Gestionar tu cuenta del Banco',
    released: '2019 - Arequipa - Perú',
    desc: 'Brindar soluciones financieras con calidad de atención, agregando valor, contribuyendo conla descentralización, ampliando nuestra cobertura de servicios y promoviendo la bancarización con inclusión social.',
    key: '2',
    },
    {
    image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTNNPwwqa9V1Uv8lCa2mLipn5T_BW9Cdf_0QK5FubdgSdYUVHLG&usqp=CAU',
    title: 'Consultar las próximas cuotas de pago',
    released: '2019 - Arequipa - Perú',
    desc: 'No olvides que una vez descargada la aplicación móvil, podrás crear tu clave de internet de seis dígitos (también la puedes generar con Multired Virtual) y acceder a tu cuenta.',
    key: '3',
    },
    {
    image: 'https://www.wikihow.com/images/thumb/e/ea/Make-a-Bank-Transfer-Payment-Step-6.jpg/v4-460px-Make-a-Bank-Transfer-Payment-Step-6.jpg.webp',
    title: 'Realizar transferencias interbancarias',
    released: '2019 - Arequipa - Perú',
    desc: 'Esta plataforma te permite pagar los procedimientos de distintas instituciones estatales, sin necesidad de ir al Banco de la Nación. Solo necesitas una tarjeta Visa, MasterCard o American Express.',
    key: '4',
    },
    {
    image: 'https://verinite.com/wp-content/uploads/2017/07/December21-bank_2665868g.jpg',
    title: 'Pagar tarjetas de crédito de otros bancos',
    released: '2019 - Arequipa - Perú',
    desc: 'Para usar Págalo.pe, descarga la aplicación y regístrate, con un usuario y contraseña. Una vez dentro de la plataforma, solo selecciona la tasa o servicio que deseas pagar, agrégala al carrito de compras y da clic en “Comprar”.',
    key: '5',
    },
])

const [list, setList] = useState([
    {
    image: 'https://png.pngtree.com/png-vector/20190130/ourlarge/pngtree-cartoon-bank-high-rise-building-elements-managementbankbluebuildingdesign-element-png-image_654272.jpg',
    key: '1',
    },
    {
    image: 'https://img2.freepng.es/20180921/hg/kisspng-clip-art-online-banking-financial-services-technol-digital-kiosk-technology-banking-industry-5ba4fc76b831e3.3892411315375391907545.jpg',
    key: '2',
    },
    {
    image: 'https://previews.123rf.com/images/ksenica/ksenica1712/ksenica171200410/91524813-el-empleado-del-banco-aconseja-al-cliente-un-hombre-y-una-mujer-detr%C3%A1s-de-un-escritorio-asesor-financi.jpg',
    key: '3',
    },
    {
    image: 'https://thumbs.dreamstime.com/b/el-consultor-del-banco-del-empleado-ayuda-al-cliente-la-muchacha-con-operaciones-financieras-94330918.jpg',
    key: '4',
    },
        {
    image: 'https://thumbs.dreamstime.com/b/empleados-y-clientes-del-banco-en-diversas-situaciones-el-consultor-aconseja-al-cliente-gente-que-se-sienta-la-cola-conseguir-106369191.jpg',
    key: '5',
    },
])

const carouselRef = useRef(null);

const {width, height} = Dimensions.get('window');

const renderItem = ({item, index}) => {
    return(
        <View>
            <TouchableOpacity
                onPress={() => {
                    carouselRef.current.scrollToIndex(index);
                    setBackground({
                        uri: item.image,
                        name: item.title,
                        stat: item.released,
                        desc: item.desc
                    })
                }}
            >
                <Image source={{uri: item.image}} style={styles.carouselImage} />
                <Text style={styles.carouselText}>{item.title}</Text>
                <MaterialCommunityIcons name='bank-transfer-in' size={40} color='white' style={styles.carouselIcon} />
            </TouchableOpacity>
        </View>
    )
}

    return (
        <ScrollView style={{backgroundColor: '#000'}}>
            <View style={styles.carouselContentContainer}>
                <View style={{...StyleSheet.absoluteFill,backgroundColor: '#000'}}>
                    <ImageBackground
                    source={{ uri: background.uri}}
                    style={styles.ImageBg}
                    blurRadius={5}>

                    <View style={styles.searchBoxContainer}>
                        <TextInput
                        placeholder ="Buscar"
                        placeholderTextColor='#666'
                        style={styles.SearchBox}>
                        </TextInput>
                        <MaterialIcons name="search" color={'#666'} size={22} style={styles.SearchBoxIcon}/>
                    </View>

                    <Text style={{ color:'white', fontSize:24, fontWeight:'bold', marginLeft:10, marginVertical:10}}>
                        Menu Principal
                    </Text>

                    <View style={styles.carouselContainerView}>
                        <Carousel style={styles.Carousel}
                            data={gallery}
                            renderItem={renderItem}
                            itemWidth={200}
                            containerWidth={width - 20}
                            separatorWidth={0}
                            ref={carouselRef}
                            inActiveOpacity={0.4}
                        />
                    </View>

                    <View style={styles.movieInfoContainer}>
                        <View style={{justifyContent: 'center'}}>
                            <Text style={styles.movieName}>{background.name}</Text>
                            <Text style={styles.movieStat}>{background.stat}</Text>
                        </View>
                        <TouchableOpacity style={styles.playIconContainer}>
                            <MaterialCommunityIcons name='bank-plus' size={24} color='#02ad94' style={{marginLeft: 4}} />
                        </TouchableOpacity>
                    </View>
                    <View style={{paddingHorizontal: 14, marginTop: 14, backgroundColor: 'rgba(106, 110, 107, 0.7)', borderRadius: 25, padding: 10, margin: 10}}>
                        <Text style={{color: 'white', opacity: 0.8, lineHeight: 20, fontSize: 20}}>
                        {background.desc}</Text>
                    </View>
                    </ImageBackground>
                </View>
            </View>

            <View style={{marginHorizontal: 14}}>
                <Text style={{color: 'white', fontSize: 24, fontWeight: 'bold', marginBottom: 24}}>
                Presupuesto participativo</Text>

                <ImageBackground
                source={{uri: 'https://besthqwallpapers.com/Uploads/18-4-2019/88002/thumb2-golden-piggy-bank-coins-money-save-money-concept-deposit.jpg'}}
                style={{height:250, width: '100%', backgroundColor: '#000'}}
                resizeMode='cover'
                >

                <Text style={{color: 'white', padding: 14, fontWeight: 'bold', fontSize: 18}}>
                Involúcrate en la asignación de recursos económicos para la realización de proyectos de tu localidad.</Text>

                <TouchableOpacity style={{...styles.playIconContainer, position: 'absolute', top: '40%', right: '40%'}}>
                    <FontAwesome5 name='play' size={22} color='#02ad94' style={{marginLeft: 4}} />
                </TouchableOpacity>
                </ImageBackground>
                <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginBottom: 24, marginTop: 36}}>
                    <Text style={{color: 'white', fontSize: 24, fontWeight: 'bold'}}>Consultar tus saldos</Text>
                    <Text style={{color: 'white', fontSize: 14, fontWeight: 'normal'}}>Ver mas</Text>
                </View>

                <FlatList
                    style={{marginBottom: 30}}
                    data={list}
                    horizontal={true}
                    keyExtractor={item => item.key}
                    renderItem={({item}) =>{
                        return(
                            <TouchableOpacity style={{marginRight: 20}}>
                                <Image source={{uri: item.image}} style={{height: 300, width: 200}}/>
                                <View style={{position: 'absolute', height: 5, width: '100%', backgroundColor: '#02ad94', opacity: 0.8}}></View>
                                <FontAwesome5 name='play' size={38} color='#fff' style={{position: 'absolute', top: '45%', left: '45%', opacity: 0.9}} />
                            </TouchableOpacity>
                        )
                    }}
                />
            </View>
        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
    },
    carouselContentContainer: {
        flex: 1,
        backgroundColor: '#000',
        height: 720, //720
        paddingHorizontal: 14
    },
    ImageBg:{
        flex: 1,
        height: null,
        width: null,
        opacity: 1,
        justifyContent: 'flex-start'
    },
    searchBoxContainer:{
        backgroundColor:'#fff',
        elevation: 10,
        borderRadius: 4,
        marginVertical: 10,
        width: '95%',
        flexDirection: 'row',
        alignSelf: 'center'
    },
    SearchBox: {
        padding: 12,
        paddingLeft: 20,
        fontSize: 16
    },
    SearchBoxIcon: {
        position: 'absolute',
        right: 20,
        top: 14,
    },
    carouselContainerView: {
        width: '100%',
        height: 300,
        justifyContent: 'center',
        alignItems: 'center'
    },
    Carousel: {
        flex: 1,
        overflow: 'visible'
    },
    carouselImage: {
        width: 200,
        height: 320,
        borderRadius: 10,
        alignSelf: 'center',
        backgroundColor: 'rgba(0,0,0,0.9)'
    },
    carouselText: {
        padding: 14,
        color: 'white',
        position: 'absolute',
        bottom: 10,
        left: 2,
        fontWeight: 'bold'
    },
    carouselIcon: {
        position: 'absolute',
        top: 15,
        right: 15
    },
    movieInfoContainer: {
        flexDirection: 'row',
        marginTop: 16,
        justifyContent: 'space-between',
        width: Dimensions.get('window').width -14,
        //backgroundColor: 'rgba(0,0,0,0.9)',
        //borderRadius: 25,
        margin: 10,
    },
    movieName: {
        paddingLeft: 14,
        color: 'white',
        fontWeight: 'bold',
        fontSize: 20,
        marginBottom: 6
    },
    movieStat: {
        paddingLeft: 14,
        color: 'white',
        fontWeight: 'bold',
        fontSize: 16,
        opacity: 0.8
    },
    playIconContainer: {
        backgroundColor: '#212121',
        padding: 10,
        borderRadius: 40,
        justifyContent: 'center',
        alignItems: 'center',
        elevation: 10,
        borderWidth: 4,
        borderColor: 'rgba(2,173,148,0.2)',
        marginBottom: 14
    }
  });

export default Home