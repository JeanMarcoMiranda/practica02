import React, {useState} from 'react'
import {View, Text, StyleSheet, Dimensions, TouchableWithoutFeedback} from 'react-native'
import Video from 'react-native-video'
import rickAndmorty from '../../rickAndmorty.mp4'
import Icon from 'react-native-vector-icons/FontAwesome'
import ProgressBar from 'react-native-progress/Bar'

const secondsToTime = (time) => {
    return ~~(time / 60) + ":" + (time % 60 < 10 ? "0" : "") + time % 60
}

const VideoScreen = () => {
    const [paused, setPaused] = useState(false)
    const [duration, setDuration] = useState(0)
    const [progress, setProgress] = useState(0)

    let player;
    const {width} = Dimensions.get("window")
    const height = width * .5625

    const handleEnd = () => {
        setPaused(true)
    }

    const handleMainButton = () => {
        setPaused(!paused)
    }

    const handleProgress = (progress) => {
        setProgress(progress.currentTime / duration)
    }

    const handleLoad = (meta) => {
        setDuration(meta.duration)
    }   

    const handleProgressPress = (e) => {
        const position = e.nativeEvent.locationX
        const progress = (position / 250) * duration
        player.seek(progress)
    }

    return(
        <View style={style.padre}>
            <View>
                <Video
                    paused={paused}
                    onProgress={handleProgress}
                    onLoad={handleLoad}
                    onEnd={handleEnd}
                    source={rickAndmorty}
                    resizeMode="cover"
                    style={{width: "100%", height: height}}
                    ref={ref => player = ref}
                />
                <View style={style.controls}>
                    <TouchableWithoutFeedback onPress={handleMainButton}>
                        <Icon name={ (paused) ? "play" : "pause"} size={30} color={"#FFF"}/>
                    </TouchableWithoutFeedback>
                    <TouchableWithoutFeedback onPress={handleProgressPress}>
                        <View>
                            <ProgressBar
                                progress={progress}
                                color="#FFF"
                                height={20}
                                width={250}
                            />
                        </View>
                    </TouchableWithoutFeedback>
                    <Text style={style.duration}>
                        {secondsToTime(Math.floor(progress * duration))}
                    </Text>
                </View>
            </View>
            <View style={{backgroundColor: '#E8E8EA', width: '100%', height: '100%', padding: 15}}>
                <Text style={{fontSize: 25, padding: 5}}>
                    Description:
                </Text>
                <Text style={{textAlign: 'justify', padding: 5}}>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellendus tenetur ipsum voluptates, mollitia quas alias. Delectus quasi error, velit nostrum dolores fuga enim quaerat soluta unde aperiam maxime cupiditate! Incidunt necessitatibus enim pariatur dicta cumque, provident sed vitae aut commodi fugiat nisi dignissimos esse doloribus? Rerum consequatur tempore, quidem eveniet provident tempora reiciendis reprehenderit, totam incidunt aut possimus amet animi excepturi eius ab deserunt fugiat error. Non earum sapiente possimus distinctio, quod quasi, temporibus numquam recusandae fugiat, voluptatum excepturi vitae aliquid laboriosam. Distinctio similique labore fugiat iste molestias minima numquam ullam facilis alias molestiae nostrum qui modi ipsa illo tempora exercitationem ipsum, maiores quo praesentium temporibus. Ducimus, id? Hic nemo assumenda exercitationem aspernatur cumque. Mollitia expedita dolore numquam laborum ratione iure quae, non hic sint molestias? Ipsa, nostrum hic voluptatibus obcaecati consequatur porro officia omnis minus eaque explicabo officiis libero maxime cumque ullam pariatur quo, inventore dolores aut dolorum et voluptate. Non quas at impedit, iste ut delectus perferendis itaque corporis repellendus tenetur nisi recusandae amet? Neque architecto voluptatum libero incidunt eaque, tenetur nulla aliquid consequatur iste. Pariatur fuga provident dignissimos voluptatem? Quo eum dolor, iure consequuntur, dolores, officia esse similique culpa ullam omnis sint beatae architecto optio. Magnam, dignissimos.
                </Text>
            </View>
        </View>
    )
}

const style = StyleSheet.create({
    padre: {
        flex: 1,
        padding: 0,
        backgroundColor: '#E8E8EA'
    },
    controls:{
        backgroundColor: 'rgba(0, 0, 0, .5)',
        width: '100%',
        height: 48,
        left:0,
        right: 0,
        bottom: 0,
        position: 'absolute',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingHorizontal: 10,
        paddingLeft: 10,
        paddingRight: 10
    },
    mainButton: {
        marginRight: 15,
    },
    duration: {
        color: '#FFF',
        marginLeft: 15
    }

})

export default VideoScreen