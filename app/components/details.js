import React from 'react'
import { View, Text, Button, StyleSheet, Image } from 'react-native'

const Lista = ({array}) => {
    const clave = array[0].charAt(0).toUpperCase() + array[0].slice(1)
    const valor = array[1]

    return(
        <View style={style.listContainer}>
            <View style={style.key}>
                <Text style={{color:'#80BD9D', fontSize: 17}}>
                    {clave}:
                </Text>
            </View>
            <View style={style.value}>
                <Text style={{color:'#F98766', fontSize: 17}}>
                    {(valor.name)
                        ? valor.name
                        : valor}
                </Text>
            </View>
        </View>
    )
}   

const Details = ({ route, navigation }) => {
    const { name, gender, image, origin, location, created } = route.params
    const datos = {name, gender, origin, location, created }
    return (
        <View style={style.padre}>
            <View style={style.containerPadre}>
                <View style={style.imageContainer}>
                    <Image
                        style={style.cover}
                        source={{ uri: image }}
                    />
                </View>
                <View style={style.contentContainer}>
                    {Object.entries(datos).map((array) => (
                        <Lista array={array}/>
                    ))}
                </View>
                <Button
                    title="Volver a la lista"
                    onPress={() => {
                        navigation.navigate('List')
                    }}
                />
            </View>
        </View>
    )
}

const style = StyleSheet.create({
    cover: {
        borderRadius: 100,
        width: 150,
        height: 150
    },
    imageContainer: {
        marginTop: 35
    },
    contentContainer: {
        color: '#FFF',
        alignItems: 'center',
        backgroundColor: '#5D6D7E',
        opacity: 0.9,
        margin: 20,
        marginTop: 40,
        marginBottom: 30,
        padding: 20,
        width: '90%',
        borderRadius: 25,
        borderWidth: 1,
        borderColor: '#5D6D7E'
    },
    listContainer: {
        marginBottom: 20,
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: '100%',
    },
    padre: {
        width: '100%',
        height: '100%',
        backgroundColor: '#FFF'
    },
    containerPadre: {
        flex: 1,
        borderRadius: 20,
        borderWidth: 3,
        borderColor: '#CCD1D1',
        margin: 30,
        marginBottom: 50,
        marginTop: 50,
        alignItems: 'center',
        paddingTop: 25,
        paddingBottom: 25,
        backgroundColor: '#D5DBDB'
    },
    key:{
        width: '40%',
        height: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    value: {
        width: '50%',
        height: 40,
        justifyContent: 'center',
    }
})

export default Details