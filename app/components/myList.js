import React, { Component } from 'react'
import { View, FlatList, Text, StyleSheet, TouchableHighlight } from 'react-native'
import axios from 'axios'
import ListItem from './myListItem'


class List extends Component {
    constructor(props) {
        super(props)

        this.state = {
            data: []
        }
    }

    componentDidMount() {
        this.getAPIData()
    }

    getAPIData = async () => {
        try {
            const response = await axios.get('https://rickandmortyapi.com/api/character/')
            this.setState({
                data: response.data.results
            })
        } catch (error) {
            console.log(error)
        }
    }

    render() {
        return (
            <View style={style.padre}>
                <View style={{width: '30%', justifyContent: 'center'}}>
                    <Text style={style.title}>Rick and Morty Characters</Text>
                </View>
                <View style={style.listContainer}>
                    <FlatList
                        style={{ height: '80%' }}
                        keyExtractor={(item) => item.id}
                        data={this.state.data}
                        renderItem={({ item, separators }) => (
                            <TouchableHighlight
                                activeOpacity={0.8}
                                underlayColor="#BFC9CA"
                                onPress={() => {
                                    this.props.navigation.navigate('Details', item)
                                }}
                                onShowUnderlay={separators.highlight}
                                onHideUnderlay={separators.unhighlight}
                                style={{
                                    borderRadius: 15,
                                    borderBottomWidth: 1,
                                    borderBottomColor: '#757575',
                                }}
                            >
                                <ListItem dataItem={item} />
                            </TouchableHighlight>
                        )}
                    />
                </View>
            </View>
        )
    }
}

const style = StyleSheet.create({
    title: {
        color: '#2ECBE4',
        fontSize: 20,
        textAlign: 'center'
    },
    listContainer: {
        width: '100%',
        height: '100%',
        padding: 5,
    },
    padre: {
        flex: 1,
        padding: 15,
        alignItems: 'center',
        width: '100%',
        height: '100%',
        backgroundColor: '#E8E8EA'
    },
    containerPadre: {
        flex: 1,
        borderRadius: 10,
        borderLeftWidth: 5,
        borderLeftColor: '#171520',
        borderTopWidth: 5,
        borderTopColor: '#171520',
        margin: 30,
        alignItems: 'center',
        paddingTop: 25,
        backgroundColor: '#3A3C48'
    },

})

export default List