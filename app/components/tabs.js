import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import AntDesign from 'react-native-vector-icons/AntDesign'

//My Components
import List from './myList'
import UserInformation from './UserInformation/UserInfo'
import Details from './details'
import TransferenceFirst from './transferences/TransferenceFirst'
import TransferenceSecond from './transferences/TransferenceSecond'
import TransferenceThird from './transferences/TransferenceThird'
import Home from './home'
import Finance from './finance'

const homeStack = createStackNavigator()

const homeStackScreen = () => {
    return(
        <homeStack.Navigator>
            <homeStack.Screen
                name="Home"
                component={Home}
                options={{
                    title: 'Settings',
                    headerStyle: {
                      backgroundColor: '#4285F4'
                    },
                    headerTintColor: '#fff',
                    headerTitleAlign:'center',
                  }}
            />
        </homeStack.Navigator>
    )
}

const financeStack = createStackNavigator()

const financeStackScreen = () => {
    return(
        <financeStack.Navigator>
            <financeStack.Screen
                name="Finance"
                component={Finance}
                options={{
                    title: 'Finance',
                    headerStyle: {
                      backgroundColor: '#4285F4'
                    },
                    headerTintColor: '#fff',
                    headerTitleAlign:'center',
                  }}
            />
        </financeStack.Navigator>
    )
}


const TransferenceStack = createStackNavigator()

const TransferenceStackScreen = () => {
   return(
      <TransferenceStack.Navigator>
         <TransferenceStack.Screen name="First" component={TransferenceFirst} 
         options={{
            title: 'Transferencia',
            headerStyle: {
               backgroundColor: '#4285F4',
            },
            headerTintColor: 'white',
            headerTitleStyle: {
               fontWeight: 'bold',
               fontSize: 20,
            },
         }}/>
         <TransferenceStack.Screen name="Second" component={TransferenceSecond} 
         options={{
            title: 'Confirmación',
            headerStyle: {
               backgroundColor: '#4285F4',
            },
            headerTintColor: 'white',
            headerTitleStyle: {
               fontWeight: 'bold',
               fontSize: 20,
            },
         }}/>
         <TransferenceStack.Screen name="Third" component={TransferenceThird} 
         options={{
            title: 'Enviado',
            headerStyle: {
               backgroundColor: '#4285F4',
            },
            headerTintColor: 'white',
            headerTitleStyle: {
               fontWeight: 'bold',
               fontSize: 20,
            },
         }}/>
      </TransferenceStack.Navigator>
   )
}

const userInfoStack = createStackNavigator()

const UserInfoStackScreen = ({userTabId}) => {
    return(
        <userInfoStack.Navigator>
            <userInfoStack.Screen
                name="UserInfo"
                component={() => <UserInformation userStackId={userTabId}/>}
                options={{ 
                    title: 'My Info',
                    headerStyle: {
                      backgroundColor: '#4285F4'
                    }, 
                    headerTintColor: '#fff',
                    headerTitleAlign:'center',
                  }}  
            />
        </userInfoStack.Navigator>
    )
}

const tabs = createMaterialBottomTabNavigator();

const Tabs = ({route}) => {
    const {userId} = route.params
    
    return (
        <tabs.Navigator
            initialRouteName="ListItems"
            activeColor='#fff'
            inactiveColor='#1E5271'
            barStyle={{ backgroundColor: '#88D958', padding: 5 }}
            shifting='true'
        >
            <tabs.Screen
                name="Home"
                component={homeStackScreen}
                options={{
                    tabBarLabel: 'Home',
                    tabBarIcon: ({ color, focused }) => (
                        <MaterialIcons name="home" color={ focused ? '#FFF' : '1E5271'} size={24} />
                    )
                }}
            />
            <tabs.Screen
                name="Finance"
                component={financeStackScreen}
                options={{
                    tabBarLabel: 'Finance',
                    tabBarIcon: ({ color, focused }) => (
                        <FontAwesome5 name="piggy-bank" color={ focused ? '#FFF' : '1E5271'} size={24} />
                    )
                }}
            />
            <tabs.Screen
                name="UserInfo"
                component={() => <UserInfoStackScreen userTabId={userId}/>}
                options={{
                    tabBarLabel: 'MyInfo',
                    tabBarIcon: ({ color, focused }) => (
                        <AntDesign name="user" color={ focused ? '#FFF' : '#1E5271'} size={25} />
                    )
                }}
            >

            </tabs.Screen>
            <tabs.Screen
                name="Transference"
                component={TransferenceStackScreen}
                options={{
                    tabBarLabel: 'Transference',
                    tabBarIcon: ({ color, focused }) => (
                        <MaterialCommunityIcons name="bank" color={ focused ? '#FFF' : '#1E5271' || color} size={20} />
                    ),
            
                }}
            />
        </tabs.Navigator>
    )
}

export default Tabs