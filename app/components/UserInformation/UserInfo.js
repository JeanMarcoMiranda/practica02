import firebase from '../../../firebase'
import 'firebase/firestore'


import React from 'react'
import {
  View, 
  Text,
  StyleSheet,
  TextInput
} from 'react-native'

const UserInformation = ({userStackId}) => {
  const [userData, setUserData] = React.useState({})
  
  React.useEffect(() => {
    const fetchData = async () => {

      const db = firebase.firestore()
      const data = await db.collection('users').get()
      
      data.docs.forEach(doc => {
        if(doc.id === userStackId){
          setUserData(doc.data())
        }
      })
    }
    fetchData()
    
  }, [])

  return(
    <View style={styles.container}>
      <View style={styles.formControl}>
        <Text style={styles.inputLabel}>
          Nombre
        </Text>
        <TextInput
          style={styles.Input}
          defaultValue={userData.name}
        />
      </View>
      <View style={styles.formControl}>
        <Text style={styles.inputLabel}>
          Email
        </Text>
        <TextInput
          style={styles.Input}
          defaultValue={userData.email}
        />
      </View>
      <View style={styles.formControl}>
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center' }}>
          <View style={{ height: '100%', width: '50%', padding: 5, paddingLeft: 0}}>
            <Text style={styles.inputLabel}>
              Número de cuenta
            </Text>
            <TextInput
              style={styles.Input}
              defaultValue={userData.accountNumber}
            />
          </View>
          <View style={{ height: '100%', width: '50%', padding: 5, paddingLeft: 0}}>
            <Text style={styles.inputLabel}>
              Monto acumulado
            </Text>
            <TextInput
              style={styles.Input}
              defaultValue={userData.bill}
            />
          </View>
        </View>
      </View>
      <View style={styles.formControl}>
        <Text style={styles.inputLabel}>
          Ultima transferencia
        </Text>
        <TextInput
          style={styles.Input}
          defaultValue={userData.lastTransfer}
        />
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    paddingVertical: 30,
    paddingHorizontal: 20
  },
  Input:{
    height: 40,
    backgroundColor: '#E7E7E7',
    textAlign: 'center',
    borderRadius: 15,
    borderWidth: 1,
    borderColor: 'grey',
    paddingBottom: 10,
    paddingTop: 10,
    marginBottom: 15,
  },

  dobleInput: {
    flex: 1,
    flexDirection: 'row',
    
  },

  formControl: {
    marginTop: 10,
    marginBottom: 10,
    height: 70
  },

  inputLabel:{
    paddingVertical: 5
  }
})

export default UserInformation