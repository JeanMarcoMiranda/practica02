import firebase from '../../../firebase'
import 'firebase/firestore'

import React, { useState } from 'react';
import {
    StyleSheet,
    TextInput,
    TouchableOpacity,
    Text,
    View,
    Image
} from 'react-native';

const Login = ({ navigation }) => {
    /*============ Setting our state ============*/
    /* Functional variables */
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    /* Style variables */
    const [emailInputStyle, setEmailInputStyle] = useState(styles.input)
    const [passwordInputStyle, setPasswordInputStyle] = useState(styles.input)


    /* Setting our functions */
    const onPressRegister = () => {
        navigation.navigate('Register')
    }

    const onPressLogin = () => {
        const db = firebase.firestore()
        db.settings({experimentalForceLongPolling: true})
        const ref = db.collection('users')
        ref.where('email', '==', email).where('password', '==', password).get()
            .then(snapshot => {
                if (snapshot.empty) {
                    console.warn('No matching documents.');
                    return;
                }

                snapshot.forEach(doc => {
                    navigation.navigate('Main', {userId : doc.id})
                });
            })
            .catch(err => {
                console.warn('Error getting documents', err);
            });
        /* navigation.navigate('Main') */
    }

    return (
        <View style={styles.padre}>
            <View style={styles.container}>
                <Image
                    style={
                        {
                            width: 150,
                            height: 150,
                        }
                    }
                    source={require('../../../img/react.png')}
                />
                <Text style={{ marginBottom: 15, fontSize: 25, color: '#fff' }}>
                    React Native
                </Text>
                <TextInput
                    style={emailInputStyle}
                    placeholder="Email"
                    placeholderTextColor='#fff'
                    defaultValue={email}
                    onFocus={() => setEmailInputStyle(styles.inputFocused)}
                    onBlur={() => setEmailInputStyle(styles.input)}
                    onChangeText={text => setEmail(text)}
                />

                <TextInput
                    style={passwordInputStyle}
                    onFocus={() => setPasswordInputStyle(styles.inputFocused)}
                    onBlur={() => setPasswordInputStyle(styles.input)}
                    placeholder="Password"
                    placeholderTextColor='#fff'
                    secureTextEntry={true}
                    defaultValue={password}
                    onChangeText={text => setPassword(text)}
                />

                {/* {
                    this.state.error.length === 0 ? null : this.state.error.map((error, index) => (
                        <Text
                            style={[styles.input, styles.error, { opacity: 1 }]}
                            key={index}>
                            {error}
                        </Text>
                    )
                    )
                } */}

                <TouchableOpacity
                    style={[styles.button]}
                    onPress={() => onPressLogin()}
                >
                    <Text
                        style={[styles.button, styles.buttonText]}
                    >
                        Ingresar
                    </Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={[{ backgroundColor: '#4758A3' }, styles.button]}
                    onPress={() => onPressRegister()}
                >
                    <Text
                        style={[styles.button, styles.buttonText]}
                    >
                        Registrarse
                    </Text>
                </TouchableOpacity>
            </View>
        </View>

    )
}

const styles = StyleSheet.create({

    padre: {
        width: '100%',
        height: '100%',
        backgroundColor: '#E8E8EA',
        paddingHorizontal: '5%',
        paddingVertical: '25%'
    },
    container: {
        flex: 1,
        height: '80%',
        width: '100%',
        borderRadius: 20,
        borderWidth: 5,
        borderColor: '#293132',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#4F5060'
    },
    input: {
        height: 50,
        width: 300,
        backgroundColor: '#4F5060',
        marginBottom: 15,
        marginTop: 15,
        paddingLeft: 25,
        borderWidth: 2,
        borderColor: '#8FAFC4',
        borderRadius: 25,
        color: '#fff',
        opacity: 0.7
    },

    inputFocused: {
        height: 50,
        width: 300,
        backgroundColor: '#4F5060',
        marginBottom: 15,
        marginTop: 15,
        paddingLeft: 25,
        borderWidth: 2,
        borderColor: '#60DBFB',
        borderRadius: 25,
        color: '#fff',
        opacity: 0.7
    },

    button: {
        width: 170,
        height: 40,
        marginTop: 25,
        backgroundColor: '#88D958',
        borderRadius: 10,
    },
    buttonText: {
        marginTop: 0,
        padding: 10,
        color: 'white',
        textAlign: 'center',
    },

    error: {
        borderWidth: 0,
        padding: 7,
        color: '#FF410D',
        alignItems: 'center',
        backgroundColor: '#F5B7B1'
    }
})

export default Login
    ;


