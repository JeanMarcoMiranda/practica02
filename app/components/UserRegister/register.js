import firebase from '../../../firebase'
import 'firebase/firestore'

import React, { useState } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity
} from 'react-native'

const Register = ({navigation}) => {
  /*============ Setting our state ============*/
  /* Functional variables */
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')

  const onPress = async () => {
    console.warn("Clicked")
    if(name === ''){
      alert('You have to fill the name at least') 
    }else{

      const db = firebase.firestore()
      db.settings({experimentalForceLongPolling: true})

      let acocuntN
      const snapshots = await db.collection('users').orderBy('accountNumber', 'desc').limit(1).get()
      snapshots.forEach(doc => {
        acocuntN = String(Number(doc.data().accountNumber) + 1)
      })
    
      await db.collection('users').add({
        name,
        email,
        password,
        bill: '0',
        accountNumber: acocuntN,
        lastTransfer: '-' 
      })

      setName('')
      setEmail('')
      setPassword('')
      navigation.navigate('Login')

    }
  }

  


  return (
    <View style={styles.container}>
      <Text style={styles.title}>
        Register
      </Text>
      <View>
        <TextInput
          style={styles.input}
          placeholder="Nombre"
          defaultValue={name}
          onChangeText={text => setName(text)}
        />
        <TextInput
          style={styles.input}
          placeholder="Email"
          defaultValue={email}
          onChangeText={text => setEmail(text)}
        />
        <TextInput
          style={styles.input}
          placeholder="Contraseña"
          secureTextEntry={true}
          defaultValue={password}
          onChangeText={text => setPassword(text)}
        />
      </View>

      <View style={{flex: 1, alignItems: 'center'}}>
        <TouchableOpacity
          style={[styles.button]}
          onPress={() => onPress()}
        >
          <Text
            style={[styles.button, styles.buttonText]}
          >
            Registrarse
                    </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    margin: 20,
    height: '90%',
    borderColor: 'red',
    borderWidth: 1,
    padding: 10
  },
  title: {
    textAlign: 'center'
  },
  input: {
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    marginBottom: 15
  },
  button:{
    backgroundColor: '#5FD700',
    paddingVertical: 5,
    borderRadius: 10,
    width: '50%',
    alignItems: 'center'
  },
  buttonText:{
    color: '#FFF',
    textAlign: 'center',
    fontStyle: 'normal',
    fontSize: 15
  }
})

export default Register