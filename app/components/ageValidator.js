import React from 'react';
import {Text, View, TextInput, StyleSheet} from 'react-native';

const AgeValidator = props => {
    return(
        <View style={style.inputContainer}>
            <View>
                <Text style={style.text}>¿Cual es su Edad?</Text>
            </View>
            <TextInput 
                style={[style.text, style.input]}
                onChangeText={props.textChange}
                value={props.value}
            />
            <View>
                <Text 
                style={textColor(props.textColor)}>
                    {props.edad}
                </Text>
            </View>
        </View>
    )
}

const style = StyleSheet.create({
    inputContainer: {
        width: '70%',
    },
    input: {
        borderWidth: 2,
        borderRadius: 15,
        borderColor: 'grey',
        height: 40,
        width: '100%',
        marginTop: 15,
        textAlign: 'center',
        backgroundColor: '#241B2F'
    },
    text: {
        textAlign: 'center',
        color: '#fff'
    },
    textEdad: {
        padding: 15,
        textAlign: 'center',
    },
})

const textColor = (color) => {
    const textEdadColor = {
        ...style.textEdad,
        color: color
    }
    return textEdadColor
}

export default AgeValidator;